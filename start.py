from selenium.webdriver.support.ui import WebDriverWait
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
import json
import os


def login(driver: webdriver, wait: WebDriverWait) -> None:
    currentDir = os.path.dirname(os.path.realpath(__file__))
    with open(currentDir + "/userData.json", 'r') as f:
        userData = json.load(f)
    f.close()
    userDataKeys = list(userData.keys())
    driver.get("https://cp.wowcircle.net/login")
    wait.until(EC.visibility_of_element_located((By.CSS_SELECTOR, '.cp-form-line input')))
    inputs = driver.find_elements(By.CSS_SELECTOR, ".cp-form-line input")
    for i in range(2):
        inputs[i].send_keys(userData[userDataKeys[i]])


def vote(driver: webdriver, wait: WebDriverWait) -> None:
    wait.until(EC.visibility_of_element_located((By.CSS_SELECTOR, '.cp-login-button')))
    driver.execute_script("document.getElementsByClassName('cp-login-button')[0].click()")
    wait.until(EC.visibility_of_element_located((By.CSS_SELECTOR, '.cp-login-button.flex-cc')))
    driver.execute_script("document.getElementsByClassName('cp-login-button flex-cc')[0].click()")
    driver.get("https://cp.wowcircle.net/vote")
    driver.execute_script(
        "let buttons = document.querySelectorAll('.table button');"
        "for (let i = 0; i < buttons.length; i++) { "
        "buttons[i].click();"
        "}"
    )


def main() -> None:
    options = webdriver.ChromeOptions()
    options.add_argument('--headless')
    driver = webdriver.Chrome(options=options)
    wait = WebDriverWait(driver, 10)
    login(driver, wait)
    vote(driver, wait)


if __name__ == "__main__":
    main()
