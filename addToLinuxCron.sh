realpath() {
  local p=$1
  if [ "${p%${p#?}}" != "/" ]; then
    p="$(pwd)/$p"
  fi
  echo "$p"
}

path=$(realpath)

cronjob="0 */12 * * * python3 ${path}start.py"
(crontab -u $USER -l; echo "$cronjob" ) | crontab -u $USER -